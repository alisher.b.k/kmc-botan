FROM python:3.8 as kmc-botan-vendor
WORKDIR /app
COPY requirements.txt .
RUN pip install --upgrade pip
RUN apt-get update \
    && apt-get -y install libpq-dev gcc
RUN pip install --user -r requirements.txt

FROM python:3.8
WORKDIR /app
COPY --from=kmc-botan-vendor /root/.local /root/.local
COPY . .

ENV PATH=/root/.local:$PATH

CMD python telegram.py
