from datetime import datetime
from model.database import Database


class BotanHistory:
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self):
        self.db = Database()
        self.table = "botan_dialog_histories"

    def getByTelegramId(self, telegram_id):
        return self.db.query(f"SELECT * FROM {self.table} WHERE telegramId = {telegram_id}")

    def addData(self, chat_id, telegram_username, telegram_first_name, telegram_last_name, request, response,
                user_email):
        return self.db.query(f"INSERT INTO  {self.table} SET chatId = '{chat_id}', "
                             f"telegramUsername = '{telegram_username}', telegramFirstName = '{telegram_first_name}', "
                             f"telegramLastName = '{telegram_last_name}', request = '{request}', "
                             f"response = '{response}', userEmail = '{user_email}', created_at = '{self.dt_string}',"
                             f"updated_at = '{self.dt_string}'")
