
from model.database import Database
from datetime import datetime


class BotanStaff:
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self):
        self.db = Database()
        self.table = "botan_staff"

    def getByFioOrPhone(self, arg):
        return self.db.query(f"SELECT * FROM {self.table} WHERE FIO like '%{arg}%' or intPhone like '%{arg}%' "
                             f"or workPhone like '%{arg}%' or mobPhone like '%{arg}%'")
