import mysql.connector
import config


class Database:
    def __init__(self):
        self.conn = mysql.connector.connect(user=config.DB_USERNAME, password=config.DB_PASSWORD,
                                            host=config.DB_HOST, database=config.DB_NAME)

    def query(self, query_string: object) -> object:
        cursor = self.conn.cursor()
        cursor.execute(query_string)
        result = cursor.fetchall()
        self.conn.commit()
        if len(result) == 0:
            return None
        else:
            return result

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.close()
