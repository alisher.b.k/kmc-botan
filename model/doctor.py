
from model.database import Database
from datetime import datetime


class Doctor:
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self):
        self.db = Database()
        self.table = "doctor"

    def getByTelegramId(self, telegram_id):
        return self.db.query(f"SELECT * FROM {self.table} WHERE telegramId = '{telegram_id}'")

    def addData(self, telegram_id, has_contract_number, contract_number, phone, clinic, doctor):
        query = f"INSERT INTO  {self.table} (telegramId, has_contract_number, contract_number, phone," \
                f" clinic, doctor, reason, created_at, updated_at)" \
                f"values ('{telegram_id}', '{has_contract_number}', '{contract_number}', '{phone}', '{clinic}'," \
                f"'{doctor}', '{doctor}', '{self.dt_string}', '{self.dt_string}')"
        return self.db.query(query)

    def update(self, list_data, condition):
        query = f"UPDATE {self.table} "
        keys = ""
        values = ""
        count = len(list_data)
        i = 0
        for key in list_data:
            if i == 0:
                keys += f"({key},"
                values += f"({list_data[key]}"
            elif i == count:
                keys += f"{key}) "
                values += f"{list_data[key]}) "
            else:
                keys += f"{key},"
                values += f"{list_data[key]},"

        query += keys + values + condition
        return self.db.query(query)
