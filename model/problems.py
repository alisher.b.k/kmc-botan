
from model.database import Database
from datetime import datetime


class Problems:
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self):
        self.db = Database()
        self.table = "problems"

    def getByTelegramId(self, telegram_id):
        return self.db.query(f"SELECT * FROM {self.table} WHERE telegramId = '{telegram_id}'")

    def getLastRecordByTelegramId(self, telegram_id):
        return self.db.query(f"SELECT * FROM {self.table} WHERE telegramId = '{telegram_id}' ORDER BY id DESC LIMIT 1")

    def addData(self, telegram_id, question, category):
        query = f"INSERT INTO  {self.table} (telegramId, question, questionFile, category, status, created_at, updated_at) " \
                f"values ('{telegram_id}', '{question}', '','{category}', 'активное', " \
                f"'{self.dt_string}', '{self.dt_string}')"
        return self.db.query(query)