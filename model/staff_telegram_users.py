from model.database import Database
from datetime import datetime


class StaffTelegramUsers:
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self):
        self.db = Database()
        self.table = "stafftelegramusers"

    def getByStaffEmail(self, arg):
        return self.db.query(f"SELECT * FROM {self.table} WHERE staffEmail = '{arg}'")

    def getByTelegramId(self, telegram_id):
        return self.db.query(f"SELECT * FROM {self.table} WHERE telegramId = '{telegram_id}'")

    def addData(self, email, telegram_id, username, first_name, last_name, update_subscription):
        return self.db.query(f"INSERT INTO  {self.table} SET staffEmail = '{email}', telegramId = '{telegram_id}', "
                             f"telegramUsername = '{username}', telegramFirstName = '{first_name}', "
                             f"telegramLastName = '{last_name}', updatesSubscription = '{update_subscription}',"
                             f"created_at = '{self.dt_string}', updated_at = '{self.dt_string}'")

    def updateData(self, email, telegram_id, username, first_name, last_name, update_subscription):
        return self.db.query(f"UPDATE {self.table} (staffEmail, telegramId, telegramUsername, telegramFirstName, "
                             f"telegramLastName, updatesSubscription, updated_at) "
                             f"VALUES('{email}', '{telegram_id}', '{username}', '{first_name}', '{last_name}', "
                             f"'{update_subscription}', '{self.dt_string}')"
                             f"WHERE telegramId = '{telegram_id}'")

    def getByName(self, arg):
        return self.db.query(f"SELECT * FROM {self.table} WHERE telegramFirstName like '%{arg}%'"
                             f" or telegramLastName like '%{arg}%'")
