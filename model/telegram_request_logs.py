
from model.database import Database
from datetime import datetime


class TelegramRequestLogs:
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")

    def __init__(self):
        self.db = Database()
        self.table = "telegram_request_logs"

    def getByTelegramId(self, telegram_id):
        return self.db.query(f"SELECT * FROM {self.table} WHERE telegramId = '{telegram_id}'")

    def addData(self, telegram_id, command, data):
        query = f"INSERT INTO  {self.table} (telegramId, command, data, created_at, updated_at)" \
                f"values ('{telegram_id}', '{command}', '{data}', '{self.dt_string}', '{self.dt_string}')"
        return self.db.query(query)

    def update(self, telegram_id, command, data, json_data):
        return self.db.query(f'UPDATE  {self.table} SET command = "{command}", data = "{data}",'
                             f"updated_at='{self.dt_string}', json_data = '{json_data}' WHERE telegramId = "
                             f"{telegram_id}")
