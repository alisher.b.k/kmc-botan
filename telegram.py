import json
import random
import urllib3
import string

import telebot
import config
import model.staff_telegram_users as staff_telegram_users

from model import problems as problems
from model import telegram_request_logs as telegram_request_logs
from model import botan_history as botan_history
from model import botan_staff as botan_staff

bot = telebot.TeleBot(config.TOKEN, parse_mode='html')
main_menu_text = "На главную"


@bot.message_handler(commands=['start'])
def welcome(message):
    users_model = staff_telegram_users.StaffTelegramUsers()
    user = users_model.getByTelegramId(telegram_id=message.chat.id)
    model = telegram_request_logs.TelegramRequestLogs()
    model.addData(telegram_id=message.chat.id, command="", data=None)
    if user is None:
        users_model.addData(email='', telegram_id=message.chat.id, username=message.chat.username,
                            first_name=message.chat.first_name, last_name=message.chat.last_name,
                            update_subscription='')
        user = users_model.getByTelegramId(telegram_id=message.chat.id)
        what_can_i_do(user=user[0], message=message)
    else:
        add_history(chat_id=message.chat.id, telegram_username=user[0][3], telegram_first_name=user[0][4],
                    telegram_last_name=user[0][5], request=message.text, email=user[0][1])
        what_can_i_do(user=user[0], message=message)


@bot.message_handler(commands=['auth'])
def auth(message):
    model = telegram_request_logs.TelegramRequestLogs()
    request_log = model.getByTelegramId(telegram_id=message.chat.id)
    if request_log is None:
        model.addData(telegram_id=message.chat.id, command="auth:email", data=None)
    else:
        model.update(telegram_id=message.chat.id, command="auth:email", data=None, json_data='{}')
    bot.send_message(chat_id=message.chat.id, text="Введите e-mail")

@bot.message_handler(commands=['pwreset'])
def pwreset(message):
    model = telegram_request_logs.TelegramRequestLogs()
    agreement_pwreset(message=message, model=model)

@bot.message_handler(commands=['booking'])
def booking(message):
    model = telegram_request_logs.TelegramRequestLogs()
    booking_start(message=message, model=model)
@bot.message_handler(commands=['feedback'])
def feedback(message):
    model = telegram_request_logs.TelegramRequestLogs()
    feedback_message(message=message, model=model)

@bot.message_handler(commands=['vacation'])
def vacation(message):
    vacation_day = random.randrange(0, 40)
    senat_day = random.randrange(0, 1)
    football_day = random.randrange(0, 3)
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    run_button = telebot.types.KeyboardButton(text=main_menu_text)
    keyboard.add(run_button)
    bot.send_message(chat_id=message.chat.id,
                     text=f"Осталось дней отпуска: {vacation_day}", reply_markup=keyboard)


@bot.message_handler(commands=['facebook'])
def facebook(message):
    model = telegram_request_logs.TelegramRequestLogs()
    model.update(telegram_id=message.chat.id, command='facebook', data=None,
                 json_data='{}')
    bot.send_message(chat_id=message.chat.id, text='Введите имя или телефон сотрудника')


@bot.message_handler(commands=['problem', 'add'])
def problem(message):
    model = telegram_request_logs.TelegramRequestLogs()
    model.update(telegram_id=message.chat.id, command='problem', data=None,
                 json_data='{}')
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    keyboard.add(telebot.types.KeyboardButton(text='Это мешает мне в работе'),
                 telebot.types.KeyboardButton(text='Этого нет в системе'))
    keyboard.add(telebot.types.KeyboardButton(text='Здесь ошибка'), telebot.types.KeyboardButton(text='Иное'))
    keyboard.add(telebot.types.KeyboardButton(text='История моих обращений'))
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    text = "Есть проблема на работе или знаешь, как улучшить работу компании? \n\n" \
           "\t\t\t\t*Сообщи в соответствующем разделе:*\n" \
           "\t\t\t\t\t\t\t\t• *«Это мешает мне в работе»* /work  - что-то *мешает твоей работе, не хватает* ресурсов " \
           "и нужного оборудования\n\n" \
           "\t\t\t\t\t\t\t\t• *«Этого нет в системе»* /system - чего-то *нет в в системах и программах:* КИАС, 1С, " \
           "MyCIC и др.\n\n" \
           "\t\t\t\t\t\t\t\t• *«Здесь ошибка»* /error - *ошибка* в данных, в информации, в документах и т.д.\n\n" \
           "\t\t\t\t\t\t\t\t• *«Иное»* /another - если ни одна из перечисленных выше кнопок не подходит для описания " \
           "твоей проблемы\n\n" \
           "\t\t\t\t*Важно! Дополни свое обращение: прикрепи документ, фото, видео, аудио.*\n" \
           "\nРазмер прикрепляемого файла не должен превышать 30 МБ"
    bot.send_message(chat_id=message.chat.id, text=text.format(), parse_mode='markdown', reply_markup=keyboard)
    return


@bot.message_handler(commands=['doctor'])
def doctor(message):
    model = telegram_request_logs.TelegramRequestLogs()
    appointment_doctor(message=message, model=model)

@bot.message_handler(commands=['centkz'])
def centkz(message):
    model = telegram_request_logs.TelegramRequestLogs()
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    main_menu_button = telebot.types.KeyboardButton(text=main_menu_text)
    other = telebot.types.KeyboardButton(text='Разное')
    tourism = telebot.types.KeyboardButton(text='Туризм')
    house = telebot.types.KeyboardButton(text='Дом')
    life = telebot.types.KeyboardButton(text='Здоровье')
    auto = telebot.types.KeyboardButton(text='Авто')
    keyboard.add(auto, life)
    keyboard.add(house, tourism)
    keyboard.add(other, main_menu_button)
    model.update(telegram_id=message.chat.id, command='cent:choose', data=None,
                 json_data='{}')
    bot.send_message(chat_id=message.chat.id, text="Добро пожаловать в интернет-магазин cent.kz\nВыберите категорию",
                     reply_markup=keyboard)


@bot.message_handler(commands=['work', 'system', 'error', 'another'])
def work(message):
    handle_work(message=message)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    model = telegram_request_logs.TelegramRequestLogs()
    requests = model.getByTelegramId(telegram_id=call.message.chat.id)

    if '#poll' in call.data:
        try:
            encoded_body = json.dumps({
                'data': call.data,
                'chat_id': call.message.chat.id,
            })
            http = urllib3.PoolManager()
            resp = http.request('POST', config.DOMAIN + '/api/saveReaction',
                                headers={'Content-Type': 'application/json'},
                                body=encoded_body)
            json_data = json.loads(resp.data)
            bot.send_message(chat_id=call.message.chat.id, text=json_data['result'])
            return
        except:
            return
    # if 'problem' in requests[0][2]:
    #     if 'work' in requests[0][2]:
    #         if call.data == 'n':
    #             print(call.data)
    #         else:
    #             print(call.data)
    #     return
    elif 'doctor:choose_doctor' in requests[0][2]:
        json_data = json.loads(requests[0][4])
        json_data.update({"doctor": call.data})
        model.update(telegram_id=call.message.chat.id, command='doctor:reason', data=requests[0][3],
                     json_data=json.dumps(json_data))
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=call.message.chat.id, text='Укажите причину вашего обращения к врачу:',
                         reply_markup=keyboard)
        return
    elif "booking:bookOrDelete_showWeekDelete" in requests[0][2]:
        callbackRoute = call.data.split('-')
        if call.data == 'null_callback':
            return
        elif callbackRoute.count:
            if 'months_list' in callbackRoute:
                return
            elif 'calendar' in callbackRoute:
                encoded_body = json.dumps({
                    'data': call.data,
                    'chat_id': call.message.chat.id,
                    'message_id': call.message.message_id
                })
                http = urllib3.PoolManager()
                resp = http.request('POST', config.DOMAIN + '/api/booking/validateCalendar',
                                    headers={'Content-Type': 'application/json'},
                                    body=encoded_body)
                json_data = json.loads(resp.data)
                keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True,
                                                                                              row_width=2)
                if 'validateDateText' in json_data:
                    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
                    bot.send_message(chat_id=call.message.chat.id, reply_markup=keyboard,
                                     text=json_data['validateDateText'])
                    return
                elif 'result' in json_data:
                    if json_data['buttons'].count:
                        keyboard.row(*json_data['buttons'])
                    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
                    bot.send_message(chat_id=call.message.chat.id, reply_markup=keyboard, text=json_data['result'])
                    return
        return
    elif 'booking:bookOrDelete_showWeek' in requests[0][2]:
        callbackRoute = call.data.split('-')
        if call.data == 'null_callback':
            return
        elif callbackRoute.count:
            if 'months_list' in callbackRoute:
                return
            elif 'calendar' in callbackRoute:
                encoded_body = json.dumps({
                    'data': call.data,
                    'chat_id': call.message.chat.id,
                    'message_id': call.message.message_id
                })
                http = urllib3.PoolManager()
                resp = http.request('POST', config.DOMAIN + '/api/booking/validateCalendar',
                                    headers={'Content-Type': 'application/json'},
                                    body=encoded_body)
                json_data = json.loads(resp.data)
                keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
                keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))

                if 'previousOrNextMonthText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['previousOrNextMonthKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                                  message_id=call.message.id,
                                                  inline_message_id=call.id, reply_markup=inline_keyboard)
                    return
                elif 'isNotEndedDateText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['isNotEndedDateKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['dataSelected'], reply_markup=keyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['isNotEndedDateText'],
                                     reply_markup=inline_keyboard)
                    return
                elif 'validateDateText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['validateDateKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['dataSelected'],
                                     reply_markup=keyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['validateDateText'],
                                     reply_markup=inline_keyboard)
                    return
                else:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['reply_markup'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)

                    bot.send_message(chat_id=call.message.chat.id, text=json_data['dataSelected'],
                                     reply_markup=keyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['result'])
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['text'], reply_markup=inline_keyboard)
                    return
            elif 'timepicker' in callbackRoute:
                keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
                keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
                encoded_body = json.dumps({
                    'data': call.data,
                    'chat_id': call.message.chat.id,
                    'first_name': call.message.chat.first_name,
                    'last_name': call.message.chat.last_name
                })
                http = urllib3.PoolManager()
                resp = http.request('POST', config.DOMAIN + '/api/booking/validateTimePicker',
                                    headers={'Content-Type': 'application/json'},
                                    body=encoded_body)
                json_data = json.loads(resp.data)
                if 'previousOrNextTimeText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['previousOrNextTimeKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                                  message_id=call.message.id,
                                                  inline_message_id=call.id, reply_markup=inline_keyboard)
                    return
                elif 'resultText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['resultKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['resultText'],
                                     reply_markup=inline_keyboard)
                    return
                elif 'theTimeIsIncorrectText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['theTimeIsIncorrectKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['theTimeIsIncorrectText'],
                                     reply_markup=inline_keyboard)
                    return
                elif 'isNotValidatedText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['isNotValidatedKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['isNotValidatedText'],
                                     reply_markup=inline_keyboard)
                    return
                elif 'timeIsBusyText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['timeIsBusyKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['timeIsBusyText'],
                                     reply_markup=inline_keyboard)
                    return
                elif 'isBusyText' in json_data:
                    arrayInlineKeyboard = constructInlineKeyboard(json_data['isBusyKeyboard'])
                    inline_keyboard = telebot.types.InlineKeyboardMarkup(arrayInlineKeyboard)
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['isBusyText'],
                                     reply_markup=inline_keyboard)
                    return
                elif 'result' in json_data:
                    bot.send_message(chat_id=call.message.chat.id, text=json_data['result'],
                                     reply_markup=keyboard)
                    return
        return
    else:
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=call.message.chat.id, text='Я еще не знаю таких команд, напишите в разделе '
                                                       f'💡 Заявить о проблеме /problem \nВ следующий раз я постараюсь ответить',
                         reply_markup=keyboard)
        return

@bot.message_handler(content_types=['document', 'photo', 'video', 'video_note', 'audio', 'voice'])
def attach(message):
    model = telegram_request_logs.TelegramRequestLogs()
    requests = model.getByTelegramId(telegram_id=message.chat.id)
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    if requests[0][2] == 'problem:answerYes':
        json_data = json.loads(requests[0][4])
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        problem = problems.Problems()
        problem.addData(telegram_id=requests[0][1], question=json_data['question'],
                            category=json_data['category'])
        problemNumber = problem.getLastRecordByTelegramId(telegram_id=requests[0][1])

        bot.send_message(chat_id=message.chat.id, text="*Спасибо, что не остался равнодушным!*"
                                                       f"\nТвое обращение №{problemNumber[0][0]} отправлено в наш центр поддержки для рассмотрения. "
                                                       "В скором времени ты получишь на него ответ.",
                         reply_markup=keyboard, parse_mode='markdown')
        bot.send_message(chat_id=message.chat.id, text="В разделе «Проблема» появилось новое обращение. "
                                                       f"\n\nАвтор: {message.chat.first_name} {message.chat.last_name}")
        return
    elif requests[0][2] == 'feedback:message':
        model.update(telegram_id=message.chat.id, command=message.text, data="", json_data="{}")
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Спасибо за твоё предложение по улучшению меня! '
            'Наша команда обязательно рассмотрит твою идею', reply_markup=keyboard)

        bot.send_message(chat_id=message.chat.id, text='В разделе «✉️ Улучшить Ботан» появилось новое обращение.'
            f'\nАвтор:{message.chat.first_name} {message.chat.last_name}', reply_markup=keyboard)
        return
    else:
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Я еще не знаю таких команд, напишите в разделе '
            f'💡 Заявить о проблеме /problem \nВ следующий раз я постараюсь ответить', reply_markup=keyboard)
    # TODO writing file to storage


@bot.message_handler(content_types=['text'])
def just_message(message):
    model = telegram_request_logs.TelegramRequestLogs()
    requests = model.getByTelegramId(telegram_id=message.chat.id)
    users_model = staff_telegram_users.StaffTelegramUsers()
    if message.text == main_menu_text or message.text == "на главную":
        model.update(telegram_id=message.chat.id, command=message.text, data="", json_data="{}")
        user = users_model.getByTelegramId(telegram_id=message.chat.id)
        what_can_i_do(user=user[0], message=message)
        return
    elif message.text == 'Это мешает мне в работе' or message.text == 'Этого нет в системе' \
            or message.text == 'Здесь ошибка' or message.text == 'Иное':
        handle_work(message=message)
        return
    elif message.text == '👩‍⚕️ Запись на прием к врачу':
        appointment_doctor(message=message, model=model)
        return
    elif message.text == '🏝 Мой отпуск':
        vacation(message=message)
        return
    elif message.text == '⌚️ Бронирование':
        booking_start(message=message, model=model)
        return
    elif message.text == '🔒️ Сброс пароля':
        agreement_pwreset(message=message, model=model)
        return
    elif message.text == '👨‍💼 Найду коллегу':
        facebook(message=message)
        return
    elif message.text == '✉️ Улучшить работу':
        feedback_message(message=message, model=model)
        return
    elif message.text == 'К категориям':
        handle_cenkz(message=message)
        return
    elif message.text == '💡 Заявить о проблеме':
        problem(message=message)
        return
    elif message.text == 'История моих обращений':
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        request = problems.Problems()
        myHistory = request.getByTelegramId(telegram_id=message.chat.id)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        if myHistory:
            for val in myHistory:
                text = f"Обращение №{val[0]} " \
                       f"\nДата обращения: {val[6]}" \
                       f"\nДата обновления: {val[7]}" \
                       f"\nКатегория: {val[4]}" \
                       f"\nСтатус: {val[5]}" \
                       f"\nДополнить обращение: /add"
                bot.send_message(chat_id=message.chat.id, text=text, reply_markup=keyboard)
            return
        else:
           bot.send_message(chat_id=message.chat.id, text='Обращения не найдены', reply_markup=keyboard)
           return

    if 'doctor' in requests[0][2]:
        handle_doctor_answer(message=message, model=model)
        return
    if 'facebook' in requests[0][2]:
        handle_facebook(message=message, requests=requests)
        return
    if 'cent' in requests[0][2]:
        handle_cenkz(message=message)
        return
    if 'pwreset' in requests[0][2]:
        handle_pwreset_answer(message=message)
        return
    if 'problem' in requests[0][2]:
        handle_problem_answer(message=message, model=model)
        return
    if 'booking' in requests[0][2]:
        booking_chooseCompany(message=message, model=model)
        return
    if 'feedback' in requests[0][2]:
        model.update(telegram_id=message.chat.id, command=message.text, data="", json_data="{}")
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Спасибо за твоё предложение по улучшению меня! '
            'Наша команда обязательно рассмотрит твою идею', reply_markup=keyboard)

        bot.send_message(chat_id=message.chat.id, text='В разделе «✉️ Улучшить Ботан» появилось новое обращение.'
            f'\nАвтор:{message.chat.first_name} {message.chat.last_name}', reply_markup=keyboard)
        return
    else:
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Я еще не знаю таких команд, напишите в разделе '
            f'💡 Заявить о проблеме /problem \nВ следующий раз я постараюсь ответить', reply_markup=keyboard)
        return


def constructInlineKeyboard(data):
    inlineKeyboard = []
    for key in data:
        subArr = []
        for i in key:
            subArr.append(
                telebot.types.InlineKeyboardButton(text=i['text'], callback_data=i['callback_data']))
        inlineKeyboard.append(subArr)
    return inlineKeyboard

def booking_start(message, model):
    model.update(telegram_id=message.chat.id, command='booking:place', data=None, json_data='{}')
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=3)
    keyboard.add(telebot.types.KeyboardButton(text='🏬 Сентрас Иншуранс'),
                 telebot.types.KeyboardButton(text='🏢 Коммеск'),
                 telebot.types.KeyboardButton(text='🏣 БЦ SАТ'))
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    bot.send_message(chat_id=message.chat.id, text='Выберите здание', reply_markup=keyboard)
    return

def booking_chooseCompany(message, model):
    requests = model.getByTelegramId(telegram_id=message.chat.id)
    if requests[0][2] == 'booking:bookOrDelete_showWeekDelete_showTimeForDelete_confirmDeleteBooking':
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        if message.text == 'Да':
            encoded_body = json.dumps({
                'chatId': message.chat.id,
            })
            http = urllib3.PoolManager()
            resp = http.request('POST', config.DOMAIN + '/api/booking/deleteBooking',
                                headers={'Content-Type': 'application/json'},
                                body=encoded_body)
            json_data = json.loads(resp.data)
            keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
            bot.send_message(chat_id=message.chat.id, text=json_data['result'], reply_markup=keyboard)
            return
        else:
            model = telegram_request_logs.TelegramRequestLogs()
            users_model = staff_telegram_users.StaffTelegramUsers()
            model.update(telegram_id=message.chat.id, command='', data="", json_data="{}")
            user = users_model.getByTelegramId(telegram_id=message.chat.id)
            what_can_i_do(user=user[0], message=message)
            return
        return
    elif requests[0][2] == 'booking:place':
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        model.update(telegram_id=message.chat.id, command='booking:chooseRoom', data=None, json_data='{}')
        if message.text == "🏬 Сентрас Иншуранс":
            keyboard.add(telebot.types.KeyboardButton(text='Приемная 2-этаж'),
                         telebot.types.KeyboardButton(text='Конференц-зал (6-этаж)'),
                         telebot.types.KeyboardButton(text='ДРР 2-этаж'),
                         telebot.types.KeyboardButton(text='ДСВ 1-этаж'),
                         telebot.types.KeyboardButton(text='ДПС 1-этаж'))
        elif message.text == '🏢 Коммеск':
            keyboard.add(telebot.types.KeyboardButton(text='БЦ - № 103'),
                         telebot.types.KeyboardButton(text='БЦ - № 203'),
                         telebot.types.KeyboardButton(text='ГО - 2 этаж'))
        else:
            keyboard.add(telebot.types.KeyboardButton(text='Академия - 207 А'),
                         telebot.types.KeyboardButton(text='Конференц - 204'))
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Выберите переговорную', reply_markup=keyboard)
        return
    elif requests[0][2] == 'booking:chooseRoom':
        model.update(telegram_id=message.chat.id, command='booking:bookOrDelete', data=None,
                     json_data=json.dumps({'room': message.text}, sort_keys=False, ensure_ascii=False,
                                          separators=(',', ': ')))
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text='Забронировать'),
                     telebot.types.KeyboardButton(text='Удалить броню'))
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Если хотите забронировать комнату переговоров '
                                                       'нажмите на кнопку «Забронировать».'
            '\n\nЕсли хотите удалить ранее сделанную броню нажмите на кнопку «Удалить броню».', reply_markup=keyboard)
        return
    elif requests[0][2] == 'booking:bookOrDelete':
        if message.text == 'Забронировать':
            encoded_body = json.dumps({
                "chatId": message.chat.id,
                "type": "create"
            })
            http = urllib3.PoolManager()
            resp = http.request('POST', config.DOMAIN + '/api/booking/showWeek',
                             headers={'Content-Type': 'application/json'},
                             body=encoded_body)

            json_data = json.loads(resp.data)
            arr = []
            for key in json_data:
                subArr = []
                for i in key:
                    subArr.append(telebot.types.InlineKeyboardButton(text=i['text'],callback_data=i['callback_data']))
                arr.append(subArr)
            inline_keyboard = telebot.types.InlineKeyboardMarkup(arr)
            bot.send_message(chat_id=message.chat.id, reply_markup=inline_keyboard, text='Выберите дату в календаре')
            return
        else:
            keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
            keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
            encoded_body = json.dumps({
                    "chatId": message.chat.id,
                    "type": "delete"
            })
            http = urllib3.PoolManager()
            resp = http.request('POST', config.DOMAIN + '/api/booking/showWeek',
                                headers={'Content-Type': 'application/json'},
                                body=encoded_body)

            json_data = json.loads(resp.data)
            arr = []
            for key in json_data:
                subArr = []
                for i in key:
                    subArr.append(telebot.types.InlineKeyboardButton(text=i['text'], callback_data=i['callback_data']))
                arr.append(subArr)
            inline_keyboard = telebot.types.InlineKeyboardMarkup(arr)
            bot.send_message(chat_id=message.chat.id, reply_markup=inline_keyboard, text='Выберите дату в календаре')
            return
    elif 'booking:bookOrDelete_showWeekDelete_showTimeForDelete' in requests[0][2]:
        encoded_body = json.dumps({
            'data': message.text,
            'chat_id': message.chat.id,
        })
        http = urllib3.PoolManager()
        resp = http.request('POST', config.DOMAIN + '/api/booking/confirmDeleteBooking',
                            headers={'Content-Type': 'application/json'},
                            body=encoded_body)
        json_data = json.loads(resp.data)
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        if json_data['buttons']:
            keyboard.add(telebot.types.KeyboardButton(text='Да'),telebot.types.KeyboardButton(text='Нет'))
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text=json_data['result'], reply_markup=keyboard)
        return
def handle_cenkz(message):
    if message.text == 'Авто':
        link = f"https://cent.kz/utls?catid=1"
        http = urllib3.PoolManager()
        resp = http.request("GET", link)
        getCentProducts(message, resp)
    elif message.text == 'Здоровье':
        link = f"https://cent.kz/utls?catid=2"
        http = urllib3.PoolManager()
        resp = http.request("GET", link)
        getCentProducts(message, resp)
    elif message.text == 'Дом':
        link = f"https://cent.kz/utls?catid=3"
        http = urllib3.PoolManager()
        resp = http.request("GET", link)
        getCentProducts(message, resp)
    elif message.text == 'Туризм':
        link = f"https://cent.kz/utls?catid=4"
        http = urllib3.PoolManager()
        resp = http.request("GET", link)
        getCentProducts(message, resp)
    elif message.text == 'Разное':
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text),
                     telebot.types.KeyboardButton(text="К категориям"))
        bot.send_message(message.chat.id, "К сожалению, на данный момент в этой категории предложений нет",
                         reply_markup=keyboard)


def what_can_i_do(user, message):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    keyboard.add(telebot.types.KeyboardButton(text='👨‍💼 Найду коллегу'),
                 telebot.types.KeyboardButton(text='🏝 Мой отпуск'))
    keyboard.add(telebot.types.KeyboardButton(text='⌚️ Бронирование'),
                 telebot.types.KeyboardButton(text='💡 Заявить о проблеме'))
    keyboard.add(telebot.types.KeyboardButton(text='👩‍⚕️ Запись на прием к врачу'),
                 telebot.types.KeyboardButton(text='🔒️ Сброс пароля'))
    keyboard.add(telebot.types.KeyboardButton(text='✉️ Улучшить работу'))
    text = f"Привет, {user[4]} {user[5]}! Я корпоративный бот группы Компаний Сентрас.\n" \
           f"Вот, что я умею: \n" \
           f"👨‍💼 Найду коллегу /facebook" \
           f"\n🏝 Мой отпуск /vacation" \
           f"\n⌚️ Бронирование /booking" \
           f"\n💡 Заявить о проблеме /problem" \
           f"\n👩‍⚕️ Запись на прием к врачу  /doctor" \
           f"\n💻️ Магазин Cent.kz  /centkz" \
           f"\n🔒️ Сброс пароля /pwreset" \
           f"\n✉️ Улучшить работу /feedback"

    bot.send_message(chat_id=message.chat.id, text=text, reply_markup=keyboard)


def feedback_message(message, model):
    model.update(telegram_id=message.chat.id, command='feedback:message', data=None, json_data='{}')
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    bot.send_message(chat_id=message.chat.id, text='"Я рад, что у тебя есть идеи как сделать меня еще лучше. 😁 '
                                                   '\nНапиши своё предложение вот здесь 👇"', reply_markup=keyboard)
    return

def handle_problem_answer(message, model):
    requests = model.getByTelegramId(telegram_id=message.chat.id)
    if requests[0][2] == "problem:answerYesOrNo":
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        json_data = json.loads(requests[0][4])
        if message.text == '✅ Да':
            model.update(telegram_id=message.chat.id, command='problem:answerYes', data=None,
                         json_data=json.dumps(json_data, sort_keys=False, ensure_ascii=False,separators=(',', ': ')))
            keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
            bot.send_message(chat_id=message.chat.id, text='Дополните обращение', reply_markup=keyboard)
            return
        else:
            model.update(telegram_id=message.chat.id, command='problem:answerNo', data=None,
                         json_data=json.dumps(json_data, sort_keys=False, ensure_ascii=False,separators=(',', ': ')))
            keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
            problem = problems.Problems()
            problem.addData(telegram_id=requests[0][1], question=json_data['question'],
                            category=json_data['category'])
            problemNumber = problem.getLastRecordByTelegramId(telegram_id=requests[0][1])
            bot.send_message(chat_id=message.chat.id, text="*Спасибо, что не остался равнодушным!*"
                f"\nТвое обращение №{problemNumber[0][0]} отправлено в наш центр поддержки для рассмотрения."
                "В скором времени ты получишь на него ответ.", reply_markup=keyboard, parse_mode='markdown')
            bot.send_message(chat_id=message.chat.id, text="В разделе «Проблема» появилось новое обращение. "
                            f"\n\nАвтор: {message.chat.first_name} {message.chat.last_name}")
            return
    elif requests[0][2] == "problem:work" or requests[0][2] == "problem:system" \
       or requests[0][2] == "problem:error" or requests[0][2] == "problem:another":
        json_data = json.loads(requests[0][4])
        json_data.update({"question": message.text})
        model.update(telegram_id=message.chat.id, command='problem:answerYesOrNo', data=None,
                     json_data=json.dumps(json_data, sort_keys=False, ensure_ascii=False,separators=(',', ': ')))
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
        keyboard.add(telebot.types.KeyboardButton(text='✅ Да'), telebot.types.KeyboardButton(text='❌ Нет'))
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Хотите прикрепить файл? Размер прикрепляемого файла не должен превышать 30 МБ',
                         reply_markup=keyboard)
        return
    else:
        bot.send_message(chat_id=message.chat.id, text='Забыли выбрать категорию обращения')
        return

def add_history(chat_id, telegram_username, telegram_first_name, telegram_last_name, request, email):
    botan_history_model = botan_history.BotanHistory()
    botan_history_model.addData(chat_id=chat_id, telegram_username=telegram_username,
                                telegram_first_name=telegram_first_name, telegram_last_name=telegram_last_name,
                                request=request, response=None, user_email=email)


def handle_work(message):
    model = telegram_request_logs.TelegramRequestLogs()
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)
    requests = model.getByTelegramId(telegram_id=message.chat.id)
    json_data = json.loads(requests[0][4])
    if message.text == "Это мешает мне в работе" or message.text == '/work':
        json_data.update({"category": 'Это мешает мне в работе'})
        model.update(telegram_id=message.chat.id, command='problem:work', data=None, json_data=json.dumps(json_data,
                     sort_keys=False, ensure_ascii=False,separators=(',', ': ')))
    elif message.text == "Этого нет в системе" or message.text == "/system":
        json_data.update({"category": 'Этого нет в системе'})
        model.update(telegram_id=message.chat.id, command="problem:system", data=None, json_data=json.dumps(json_data,
                     sort_keys=False, ensure_ascii=False,separators=(',', ': ')))
    elif message.text == "Здесь ошибка" or message.text == "/error":
        json_data.update({"category": 'Здесь ошибка'})
        model.update(telegram_id=message.chat.id, command="problem:error", data=None, json_data=json.dumps(json_data,
                     sort_keys=False, ensure_ascii=False,separators=(',', ': ')))
    else:
        json_data.update({"category": 'Иное'})
        model.update(telegram_id=message.chat.id, command="problem:another", data=None, json_data=json.dumps(json_data,
                     sort_keys=False, ensure_ascii=False,separators=(',', ': ')))
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    bot.send_message(chat_id=message.chat.id, text='Введите ваше сообщение', reply_markup=keyboard)

def handle_attach_answer(message):
    print("handle_attach_answer: "+message.text)

def agreement_pwreset(message, model):
    model.update(telegram_id=message.chat.id, command='pwreset:agreed', data=None, json_data='{}')
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    keyboard.add(telebot.types.KeyboardButton(text='✅ Да'), telebot.types.KeyboardButton(text='❌ Нет'))
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    bot.send_message(chat_id=message.chat.id, text='Вы уверены что хотите сбросить пароль?', reply_markup=keyboard)

def handle_pwreset_answer(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    if message.text == '✅ Да':
        letters_and_digits = string.ascii_letters + string.digits
        rand_string = ''.join(random.sample(letters_and_digits, 8))
        bot.send_message(chat_id=message.chat.id, text="Ваш новый пароль : " + rand_string, reply_markup=keyboard)
    if message.text == '❌ Нет':
        model = telegram_request_logs.TelegramRequestLogs()
        users_model = staff_telegram_users.StaffTelegramUsers()
        model.update(telegram_id=message.chat.id, command='', data="", json_data="{}")
        user = users_model.getByTelegramId(telegram_id=message.chat.id)
        what_can_i_do(user=user[0], message=message)
        return

def appointment_doctor(message, model):
    model.update(telegram_id=message.chat.id, command='doctor:insured', data=None, json_data='{}')
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    keyboard.add(telebot.types.KeyboardButton(text='✅ Да'), telebot.types.KeyboardButton(text='❌ Нет'))
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    bot.send_message(chat_id=message.chat.id, text='Вы застрахованы ?', reply_markup=keyboard)


def handle_doctor_answer(message, model):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
    requests = model.getByTelegramId(telegram_id=message.chat.id)
    if message.text == '✅ Да':
        json_data = json.loads(requests[0][4])
        json_data.update({"has_contract_number": 'yes'})
        model.update(telegram_id=message.chat.id, command='doctor:contract_number', data=None,
                     json_data=json.dumps(json_data))
        bot.send_message(chat_id=message.chat.id, text='Введите номер карточки страхования:', reply_markup=keyboard)
        return
    elif message.text == '❌ Нет':
        json_data = json.loads(requests[0][4])
        json_data.update({"has_contract_number": 'no'})
        model.update(telegram_id=message.chat.id, command='doctor:phone', data=None, json_data=json.dumps(json_data))
        bot.send_message(chat_id=message.chat.id, text='Укажите свой контактный номер телефона в виде: +7XXXXXXXXXX',
                         reply_markup=keyboard)
        return
    elif message.text == '👩‍⚕️ Запись на прием к врачу':
        appointment_doctor(message=message, model=model)
        return

    if requests[0][2] == 'doctor:work':
        handle_problem_answer(message=message, model=model)
        return
    elif requests[0][2] == 'doctor:attach':
        handle_attach_answer(message=message)
        return
    elif requests[0][2] == 'doctor:contract_number':
        if len(message.text) < 8:
            bot.send_message(chat_id=message.chat.id, text='Введите корректный номер страховой карточки:')
            return
        json_data = json.loads(requests[0][4])
        json_data.update({"contract_number": message.text})
        model.update(telegram_id=message.chat.id, command='doctor:phone', data=message.text,
                     json_data=json.dumps(json_data))
        keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)
        keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
        bot.send_message(chat_id=message.chat.id, text='Укажите свой контактный номер телефона в виде: +7XXXXXXXXXX',
                         reply_markup=keyboard)
        return
    elif requests[0][2] == 'doctor:phone':
        choose_clinic(model=model, message=message, keyboard=keyboard, requests=requests)
        return
    elif requests[0][2] == 'doctor:clinic':
        json_data = json.loads(requests[0][4])
        if message.text == "🏥 МЦ на Шарипова":
            clinic = 1
        else:
            clinic = 2
        json_data.update({"clinic": clinic})
        model.update(telegram_id=message.chat.id, command='doctor:choose_doctor', data=requests[0][3],
                     json_data=json.dumps(json_data))
        inline_keyboard = telebot.types.InlineKeyboardMarkup(row_width=2)
        btn1 = telebot.types.InlineKeyboardButton(text='Хирург', callback_data='d:x')
        btn2 = telebot.types.InlineKeyboardButton(text='Уролог', callback_data='d:u')
        btn3 = telebot.types.InlineKeyboardButton(text='Стоматолог', callback_data='d:s')
        btn4 = telebot.types.InlineKeyboardButton(text='Детский пульмонолог', callback_data='d:d')
        btn5 = telebot.types.InlineKeyboardButton(text='Педиатр', callback_data='d:p')
        btn6 = telebot.types.InlineKeyboardButton(text='Гастроэнтеролог', callback_data='d:g')
        btn7 = telebot.types.InlineKeyboardButton(text='Эндокринолог', callback_data='d:e')
        btn8 = telebot.types.InlineKeyboardButton(text='Дерматолог', callback_data='d:de')
        btn9 = telebot.types.InlineKeyboardButton(text='Кардиолог', callback_data='d:k')
        btn10 = telebot.types.InlineKeyboardButton(text='Терапевт', callback_data='d:t')
        btn11 = telebot.types.InlineKeyboardButton(text='Гинеколог', callback_data='d:gi')
        btn12 = telebot.types.InlineKeyboardButton(text='ЛОР', callback_data='d:l')
        btn13 = telebot.types.InlineKeyboardButton(text='Невропатолог', callback_data='d:n')
        btn14 = telebot.types.InlineKeyboardButton(text='Детский невропатолог', callback_data='d:dn')
        btn15 = telebot.types.InlineKeyboardButton(text='Окулист', callback_data='d:o')
        inline_keyboard.add(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12, btn13, btn14,
                            btn15)
        bot.send_message(chat_id=message.chat.id, text='Специалисты:',
                         reply_markup=inline_keyboard)
        return
    elif requests[0][2] == 'doctor:reason':
        json_data = json.loads(requests[0][4])
        json_data.update({"reason": message.text})
        model.update(telegram_id=message.chat.id, command='doctor:last', data=requests[0][3]+","+message.text,
                     json_data=json.dumps(json_data))
        keyboard.add(telebot.types.KeyboardButton(text='Отправить'))
        bot.send_message(chat_id=message.chat.id, text='Если данные верны нажмите кнопку "Отправить"',
                         reply_markup=keyboard)
        return
    elif requests[0][2] == 'doctor:last':
        if message.text == 'Отправить':
            json_data = json.loads(requests[0][4])
            has_contract_number = "Нет"
            data = f"{requests[0][3]}".split(',')
            if json_data['has_contract_number'] == "yes":
                has_contract_number = "Да\nНомер страховой карточки: " + data[0]
            if json_data['clinic'] == 1:
                clinic = "МЦ на Шарипова"
            else:
                clinic = "МЦ на Достык"
            text = "Имя: " + f"{message.chat.first_name}" + \
                   "\nФамилия: " + f"{message.chat.last_name}" + \
                   "\nКонтактный номер: " + f"{json_data['phone']}".encode('utf8').decode() + \
                   "\nКлиника: " + f"{clinic}" + \
                   "\nВрач: " + f"{get_doctor(json_data['doctor'])}" + \
                   "\nПричина обращения: " + f"{data[1]}" + \
                   "\nЗастрахован(a): " + f"{has_contract_number}"
            bot.send_message(message.chat.id, text)
            keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=1)
            keyboard.add(telebot.types.KeyboardButton(text=main_menu_text))
            bot.send_message(message.chat.id, "Ваша заявка принята!", reply_markup=keyboard)
            # bot.send_message(message.chat.id, "Спасибо за обращение! Пожалуйста, дождитесь звонка от сотрудника "
            #                                   "call-центра с информацией о вашей записи.", reply_markup=keyboard)


def get_doctor(key):
    if key == "d:x":
        return "Хирург"
    elif key == 'd:u':
        return "Уролог"
    elif key == 'd:s':
        return "Стоматолог"
    elif key == 'd:d':
        return "Детский пульмонолог"
    elif key == 'd:p':
        return "Педиатр"
    elif key == 'd:g':
        return "Гастроэнтеролог"
    elif key == 'd:e':
        return "Эндокринолог"
    elif key == 'd:de':
        return "Дерматолог"
    elif key == 'd:k':
        return "Кардиолог"
    elif key == 'd:t':
        return "Терапевт"
    elif key == 'd:gi':
        return "Гинеколог"
    elif key == 'd:l':
        return "ЛОР"
    elif key == 'd:n':
        return "Невропатолог"
    elif key == 'd:dn':
        return "Детский невропатолог"
    elif key == 'd:o':
        return "Окулист"


def choose_clinic(model, message, keyboard, requests):
    json_data = json.loads(requests[0][4])
    json_data.update({"phone": message.text})
    model.update(telegram_id=message.chat.id, command='doctor:clinic', data=requests[0][3],
                 json_data=json.dumps(json_data))
    keyboard.add(telebot.types.KeyboardButton(text='🏥 МЦ на Шарипова'))
    keyboard.add(telebot.types.KeyboardButton(text='🏥 МЦ на Достык'))
    bot.send_message(chat_id=message.chat.id, text='Выберите клинику:', reply_markup=keyboard)


def handle_facebook(message, requests):
    botan_staff_model = botan_staff.BotanStaff()
    if requests[0][2] == 'facebook':
        if len(message.text) < 3:
            bot.send_message(message.chat.id, "Количество символов должно быть больше 3")
            return
        result = botan_staff_model.getByFioOrPhone(message.text)
        if result is None:
            bot.send_message(message.chat.id, "Сотрудники с данным именем или телефоном не найдены")
            return
        for key in result:
            text = f"ФИО: {key[1]}\nМесто работы: {key[2]}\nМобильный телефон: {key[6]}\nEmail: {key[7]}"
            users_model = staff_telegram_users.StaffTelegramUsers()
            if key[10] is not None:
                user = users_model.getByStaffEmail(key[10])
                if user is None:
                    user = users_model.getByStaffEmail(key[7])
                sendUserData(user, message, key, text)
            else:
                if key[11] is not None:
                    user = users_model.getByTelegramId(telegram_id=key[11])
                    sendUserData(user, message, key, text)
                else:
                    bot.send_message(message.chat.id, text)
        return


def sendUserData(user, message, key, text):
    if user is not None and user[0][4] is not None:
        inline_keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
        btn1 = telebot.types.InlineKeyboardButton(text=f"Написать {key[1]}", url=f"https://t.me/{user[0][3]}")
        inline_keyboard.add(btn1)
        bot.send_message(message.chat.id, text, reply_markup=inline_keyboard)
    else:
        bot.send_message(message.chat.id, text)


def getCentProducts(message, resp):
    json_data = json.loads(resp.data)
    for key in json_data:
        inline_keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
        btn1 = telebot.types.InlineKeyboardButton(text=key['botanbutton'], url=key['link'])
        inline_keyboard.add(btn1)
        bot.send_photo(message.chat.id, photo=key['fname'], caption=key['desk'],
                       reply_markup=inline_keyboard)

if __name__ == '__main__':
    bot.polling(none_stop=True, interval=0)
